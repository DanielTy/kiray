package kiray.com.domains;


import kiray.com.security.User;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class House {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int price;
    @ManyToOne
    private User lesser;
    private String location;

    private String detail;
    
    private String photo;



}
