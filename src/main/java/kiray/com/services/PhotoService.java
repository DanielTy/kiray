package kiray.com.services;

import kiray.com.domains.Photo;
import org.springframework.stereotype.Service;

@Service
public interface PhotoService {

    Photo findPhotoById(Long id);
}
