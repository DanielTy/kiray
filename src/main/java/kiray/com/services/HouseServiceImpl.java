package kiray.com.services;

import kiray.com.domains.House;
import kiray.com.repositories.HouseRepository;
import kiray.com.security.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HouseServiceImpl implements HouseService {

    private HouseRepository houseRepository;

    @Autowired
    public HouseServiceImpl(HouseRepository houseRepository){
        this.houseRepository = houseRepository;
    }



    @Override
    public House houseById(long id) {
        return houseRepository.findById(id).get();
    }

    @Override
    public List<House> findAllHouses() {
        return houseRepository.findAll();
    }

    @Override
    public void addHouse(House house) {
        houseRepository.save(house);
    }

    @Override
    public void deleteHouse(House house) {
        houseRepository.delete(house);
    }

    @Override
    public List<House> searchHouse(int greater, int less) {
        return houseRepository.findByPriceGreaterThanEqualOrPriceLessThanEqual(greater, less);
    }



	@Override
	public List<House> findBylesser(User lesser) {
		
		return houseRepository.findByLesser(lesser);
	}




}
