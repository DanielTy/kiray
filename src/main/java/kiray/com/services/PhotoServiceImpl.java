package kiray.com.services;

import kiray.com.domains.Photo;
import kiray.com.repositories.PhotoRepository;
import org.springframework.stereotype.Service;

@Service
public class PhotoServiceImpl  implements PhotoService{
    private final PhotoRepository photoRepository;

    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Photo findPhotoById(Long id) {
        return photoRepository.findById(id).get();
    }
}
