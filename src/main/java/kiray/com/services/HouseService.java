package kiray.com.services;

import java.util.List;

import org.springframework.stereotype.Service;

import kiray.com.domains.House;
import kiray.com.security.User;

@Service
public interface HouseService {

    House houseById(long id);
    List<House> findAllHouses();
    void addHouse(House house);
    void deleteHouse(House house);

    List<House> searchHouse(int greater, int less);
    List<House> findBylesser(User lesser);


}
