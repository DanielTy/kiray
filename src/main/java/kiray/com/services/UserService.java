package kiray.com.services;

import kiray.com.security.Role;
import org.springframework.security.core.userdetails.UserDetailsService;

import kiray.com.security.User;

import java.util.List;

public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user, String role);
	void deleteUser(User user);
	User findUserById(Long id);
	Iterable<User> findAllUsers();
	
}
