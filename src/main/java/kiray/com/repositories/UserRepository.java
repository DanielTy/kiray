package kiray.com.repositories;

import org.springframework.data.repository.CrudRepository;

import kiray.com.security.User;

public interface UserRepository extends CrudRepository<User, Long>{
	
	User findByUsername(String username);
	void deleteUserById(Long id);


}
