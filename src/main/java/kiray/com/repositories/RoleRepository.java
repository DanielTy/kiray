package kiray.com.repositories;

import org.springframework.data.repository.CrudRepository;

import kiray.com.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

	 Role findByRole(String role);
	 
}
