package kiray.com.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import kiray.com.domains.House;
import kiray.com.security.User;
@Repository
public interface HouseRepository extends CrudRepository<House, Long> {

    @Override
    Optional<House> findById(Long aLong);

    List<House> findAll();
    
    
    List<House> findByPriceGreaterThanEqualOrPriceLessThanEqual(int greaterPrice, int lessPrice);
    List<House> findByLesser(User lessor);

}
