package kiray.com.repositories;

import kiray.com.domains.Photo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface PhotoRepository extends CrudRepository<Photo, Long> {

    Optional<Photo> findById(Long id);
}
