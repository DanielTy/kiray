//package kiray.com.repositories;
//
//import kiray.com.domains.Comment;
//import kiray.com.security.User;
//import org.springframework.data.repository.CrudRepository;
//
//import java.util.List;
//
//public interface CommentRepository extends CrudRepository<Comment,Long> {
//
//    List<Comment> findByPost(User user);
//    Comment findByCreator(User creator);
//    int countByPost(User user);
//    Comment findById(long id);
//    List<Comment> findByPostOrderByDateCreated(User user);
//    List<Comment> findTop5ByPostOrderByDateCreatedDesc(User user);
//
//}
