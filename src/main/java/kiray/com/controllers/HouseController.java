package kiray.com.controllers;

import kiray.com.configurations.AppConstants;
import kiray.com.domains.House;
import kiray.com.domains.Photo;
import kiray.com.security.User;
import kiray.com.services.FileStorageService;
import kiray.com.services.HouseService;
import kiray.com.services.PhotoService;
import kiray.com.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;

import javax.validation.Valid;

@Controller
public class HouseController {

    private final HouseService houseService;
    private final UserService userService;
    private final PhotoService photoService;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    public HouseController(HouseService houseService, UserService userService, PhotoService photoService) {
        this.houseService = houseService;
        this.userService = userService;
        this.photoService = photoService;
    }
    @ModelAttribute("house")
    public House newHouse(Model model) {
    	return new House();
    }

    @GetMapping("/house")
    public String getHouse(Model model){
        model.addAttribute("houses", houseService.findAllHouses());
        return "home";
    }
    @GetMapping("/delete/{id}")
    public String deleteHouse(@PathVariable Long id) {
    	House house = houseService.houseById(id);
    	houseService.deleteHouse(house);
    	return "redirect:/house";
    }

    @GetMapping("/housebylessor")
    public String findBylesser(Model model, @AuthenticationPrincipal UserDetails details){
        model.addAttribute("houses", houseService.findBylesser(userService.findUserByUsername(details.getUsername())));
        return "order_form";
        
    }
    @PostMapping("/house/search")
    public String searchHouse(Model model, @RequestParam(name = "greater") int greater, @RequestParam(name = "less") int less){
        model.addAttribute("houses", houseService.searchHouse(greater, less));
        return "home";
    }

    @GetMapping("/addHouse")
    public String getHouse(House house,@RequestParam("file") MultipartFile file, Model model, @AuthenticationPrincipal UserDetails userDetails){
    	String role =  userService.findUserByUsername(userDetails.getUsername()).getRoles().toString();

        if (role.equals("[Role(id=1, role=LESSOR)]")){
            return "Admin";

        }else if (role.equals("[Role(id=2, role=ADMIN)]")){

            return "order_form";
            
        }
        return null;
    }


    @PostMapping("/addHouse")
    public String addHouse(@RequestParam("file") MultipartFile file, @Valid House house, Model model, @AuthenticationPrincipal UserDetails userDetails) throws IOException{

        User lesser = userService.findUserByUsername(userDetails.getUsername());
        house.setLesser(lesser);
        String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(AppConstants.DOWNLOAD_PATH)
				.path(fileName).toUriString();
		house.setPhoto(fileDownloadUri);
        houseService.addHouse(house);


        return "redirect:/house";
    }
}
