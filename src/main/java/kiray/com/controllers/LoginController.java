package kiray.com.controllers;

import javax.validation.Valid;

import kiray.com.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import kiray.com.security.User;
import kiray.com.services.UserService;

import java.util.List;

@Controller
public class LoginController {
	
	@Autowired
    private UserService userService;
	
	@ModelAttribute(name="user")
	public User user() {
		return new User();
	}

    @GetMapping("/login")
    public String login(){
        return "login";
    }


    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String createNewUser(@Valid User user, BindingResult bindingResult, Model model, @RequestParam String role) {
        User userExists = userService.findUserByUsername(user.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("user", "error.user",
                            "There is already a user registered with the username provided");
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        } else {
        	
            userService.saveUser(user, role);
            
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "registration";
        }
    }
    
    @GetMapping("/access_pass")
    public String accessDenied(){
        return "access_pass";
    }

    @GetMapping("/deleteUser/{id}")
    public String deleteUser(@PathVariable Long id){
	    User user = userService.findUserById(id);
	    userService.deleteUser(user);
	    return "redirect:/admin";
    }

    @GetMapping("/test")
    public String test(Model model){
        Iterable<User> user = userService.findAllUsers();
        model.addAttribute("users", user);
        return "test";
    }

}
