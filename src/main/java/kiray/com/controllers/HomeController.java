package kiray.com.controllers;

import kiray.com.security.User;
import kiray.com.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller        
public class HomeController {

  private final UserService userService;

  public HomeController(UserService userService) {
    this.userService = userService;
  }

/*  @GetMapping("/home")
  public String home() {
	 // throw new RuntimeException("Test Exception");
    return "home";    
  }*/
  
  @GetMapping("/admin")
  public String testSec(Model model) {
    Iterable<User> user = userService.findAllUsers();
    model.addAttribute("users", user);
    return "Admin";
  }

}
