package kiray.com.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller            
public class managerController {

  @GetMapping("/manager")    
  public String about() {
     
    return "manager";
  }
  
}
