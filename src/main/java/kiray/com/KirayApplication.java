package kiray.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KirayApplication {

	public static void main(String[] args) {
		SpringApplication.run(KirayApplication.class, args);
	}

}

