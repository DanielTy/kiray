package com.daniel.tacoclient;

import kiray.com.controllers.HouseController;
import kiray.com.controllers.managerController;
import kiray.com.domains.House;
import kiray.com.services.HouseService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@RunWith(SpringRunner.class)
@WebMvcTest(HouseController.class)
public class HouseControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private HouseService houseService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetHouse() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/house"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("home"))
                .andExpect(MockMvcResultMatchers.view().name("home"))
                .andDo(print());
    }
    @Test
    public void testFindByLesser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/houseByLesser"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("order_form"))
                .andExpect(MockMvcResultMatchers.view().name("order_form"))
                .andDo(print());
    }
    @Test
    public void testAddHouser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/addHouse"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("order_form"))
                .andExpect(MockMvcResultMatchers.view().name("order_form"))
                .andDo(print());
    }
    @Test
    public void testDeleteHouse(){
        House house = houseService.houseById(1);
        houseService.deleteHouse(house);

    }


}
